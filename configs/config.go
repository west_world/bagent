package configs

import (
	"bufio"
	"errors"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
)

type BagentConf struct {
	Os         string
	Osid       string
	Key        string
	Listen     string
	ListenPort int
	BserverIP  string
}

type BkeyConf struct {
}

var BagentConfAddr string
var BparaAddr string
var BkeyAddr string
var ScriptsDir string
var Config BagentConf

func (c *BagentConf) Init() error {
	f, err := os.Open(BagentConfAddr)
	if err != nil {
		return err
	}
	buf := bufio.NewReader(f)
	for {
		line, err := buf.ReadString('\n')
		if err != nil || io.EOF == err {
			if err == io.EOF {
				return nil
			}
			return err
		}

		line = strings.TrimSpace(line)
		if strings.HasPrefix(line, "#") {
			continue
		}
		if len(line) == 0 {

			return errors.New("config format error")
		}
		splits := strings.Split(line, "=")
		if len(splits) < 2 {
			log.Println("shutting down")
			return errors.New("config format error")
		}
		if len(splits[0]) == 0 || len(splits[1]) == 0 {
			return errors.New("config format error")
		}
		switch splits[0] {
		case "os":
			c.Os = splits[1]
		case "osid":
			c.Osid = splits[1]
		case "key":
			c.Key = splits[1]
		case "listen":
			c.Listen = splits[1]
		case "listen_port":
			port, convErr := strconv.Atoi(splits[1])
			if convErr != nil {
				return errors.New("port should be a number")
			}
			c.ListenPort = port
		case "bserver_ip":
			c.BserverIP = splits[1]
			// todo include file
		}
	}
}
