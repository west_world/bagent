package handlers

import (
	"encoding/json"
	"net/http"
	"strings"

	"bagent/commons"
	"fmt"
)

type BcmdResp struct {
	Code   int64  `json:"code"`
	Msg    string `json:"msg"`
	Stdout string `json:"stdout"`
	Stderr string `json:"stderr,omitempty"`
	Err    string `json:"err,omitempty"`
}

type BcmdReq struct {
	Cmd string
}

// 处理 bcmd 请求
func BcmdHandler(w http.ResponseWriter, r *http.Request) {

	resp := &BcmdResp{}
	decoder := json.NewDecoder(r.Body)
	var bcmd BcmdReq
	err := decoder.Decode(&bcmd)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		resp.Code = 301
		resp.Msg = "bad post fmt"
		return
	}

	ip := strings.Split(r.RemoteAddr, ":")[0]
	if _, ok := Bpara.AllowIP[ip]; !ok {
		w.WriteHeader(http.StatusBadRequest)
		resp.Code = 307
		resp.Msg = fmt.Sprintf("ip %v not allowed", ip)
		j, _ := json.Marshal(resp)
		w.Write([]byte(j))
		return
	}

	splits := strings.Split(bcmd.Cmd, " ")
	cmd := splits[0]

	if _, ok := Bpara.AllowCmd[cmd]; !ok {
		w.WriteHeader(http.StatusBadRequest)
		resp.Code = 305
		resp.Msg = "invalid command"
		j, _ := json.Marshal(resp)
		w.Write([]byte(j))
		return
	}
	fmt.Printf("cmd %v\n", cmd)
	if cmd == "wget" {
		if len(splits) != 3 {
			w.WriteHeader(http.StatusBadRequest)
			resp.Code = 305
			resp.Msg = "invalid command"
			j, _ := json.Marshal(resp)
			w.Write([]byte(j))
			return
		} else {
			opt := strings.TrimSpace(splits[1])
			dir := strings.TrimSpace(splits[2])
			url := strings.TrimSpace(splits[3])
			if !strings.HasPrefix(opt, "-") {
				w.WriteHeader(http.StatusBadRequest)
				resp.Code = 305
				resp.Msg = "invalid command"
				j, _ := json.Marshal(resp)
				w.Write([]byte(j))
				return
			}
			if !MatchWorkdir(dir) {
				fmt.Printf("%v check", dir)
				w.WriteHeader(http.StatusBadRequest)
				resp.Code = 306
				resp.Msg = "not permitted path"
				j, _ := json.Marshal(resp)
				w.Write([]byte(j))
				return
			}
			if !(strings.HasPrefix(url, "http://") || strings.HasPrefix(url, "https://")) {
				w.WriteHeader(http.StatusBadRequest)
				resp.Code = 307
				resp.Msg = "no url"
				j, _ := json.Marshal(resp)
				w.Write([]byte(j))
				return
			}
		}
	} else {
		// not wget
		if len(splits) > 1 {
			for _, split := range splits[1:] {
				if strings.HasPrefix(split, "-") {
					continue
				}
				if !MatchWorkdir(split) {
					resp.Code = 306
					resp.Msg = "not permitted path"
					j, _ := json.Marshal(resp)
					w.Write([]byte(j))
					return
				}
			}
		}
	}

	w.WriteHeader(http.StatusOK)
	ret := commons.RunCommand(bcmd.Cmd, Bpara.ScriptTimeout)
	resp.Code = 200
	resp.Msg = "ok"
	resp.Stdout = ret.Stdout
	resp.Stderr = ret.Stderr
	resp.Err = ret.Error

	j, _ := json.Marshal(resp)
	w.Write([]byte(j))
}

func MatchAllowIP(ip string) bool {
	if _, ok := Bpara.AllowIP["*"]; ok {
		return true
	}
	if _, ok := Bpara.AllowIP[ip]; ok {
		return true
	}
	return false
}
