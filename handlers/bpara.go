package handlers

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
	"time"
)

type BparaType struct {
	WorkDir       map[string]struct{}
	ScriptTimeout time.Duration
	BcmdPath      map[string]struct{}
	AllowCmd      map[string]struct{}
	AllowIP       map[string]struct{}
}

var Bpara = BparaType{}

// 解析 Key.cnf 文件
func ParseBparaConf(addr string) (err error) {

	fi, err := os.Open(addr)
	if err != nil {
		fmt.Printf("Error: %s\n", err)
		return err
	}
	defer fi.Close()

	br := bufio.NewReader(fi)
	for {
		l, _, c := br.ReadLine()
		if c == io.EOF {
			break
		}
		line := string(l)
		line = strings.TrimSpace(line)
		if strings.HasPrefix(line, "#") {
			continue
		}
		splits := strings.Split(line, "=")
		if len(splits) != 2 {
			fmt.Printf("Error: fmt err of %v\n", line)
			continue
		}

		if len(splits[0]) == 0 || len(splits[1]) == 0 {
			return errors.New("bpara format error")
		}

		switch splits[0] {
		case "work_dir":
			Bpara.WorkDir = map[string]struct{}{}
			for _, split := range strings.Split(splits[1], ",") {
				Bpara.WorkDir[split] = struct{}{}
			}
		case "script_timeout":
			intervalSeconds, err := strconv.ParseInt(splits[1], 10, 32)
			if err != nil {
				fmt.Printf("Error: fmt err of %v\n", line)
				continue
			}
			interval := time.Duration(intervalSeconds) * time.Second
			Bpara.ScriptTimeout = interval
		case "bcmd_path":
			Bpara.BcmdPath = map[string]struct{}{}
			for _, split := range strings.Split(splits[1], ",") {
				Bpara.BcmdPath[split] = struct{}{}
			}
		case "allow_cmd":
			Bpara.AllowCmd = map[string]struct{}{}
			for _, split := range strings.Split(splits[1], ",") {
				Bpara.AllowCmd[split] = struct{}{}
			}
		case "allow_ip":
			Bpara.AllowIP = map[string]struct{}{}
			for _, split := range strings.Split(splits[1], ",") {
				Bpara.AllowIP[split] = struct{}{}
			}
		}
	}
	return nil
}
