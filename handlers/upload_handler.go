package handlers

import (
	"crypto/rand"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
)

const maxUploadSize = 2 * 1024 * 1024 // max file size: 2GB

type C2BResp struct {
	Code int64  `json:"code"`
	Msg  string `json:"msg"`
	Err  string `json:"err,omitempty"`
}

func UploadFunc(w http.ResponseWriter, r *http.Request) {

	resp := &C2BResp{}

	// validate file size
	r.Body = http.MaxBytesReader(w, r.Body, maxUploadSize)
	if err := r.ParseMultipartForm(maxUploadSize); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		resp.Code = 309
		resp.Msg = "file size greater than 2GB"
		j, _ := json.Marshal(resp)
		w.Write([]byte(j))
		return
	}

	// parse and validate file and post parameters
	//fileType := r.PostFormValue("type")
	file, _, err := r.FormFile("uploadFile")
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		resp.Code = 310
		resp.Msg = "invalid file"
		j, _ := json.Marshal(resp)
		w.Write([]byte(j))
		return
	}
	defer file.Close()
	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		resp.Code = 314
		resp.Msg = "invalid file"
		j, _ := json.Marshal(resp)
		w.Write([]byte(j))
		//renderError(w, "INVALID_FILE", http.StatusBadRequest)
		return
	}

	fileName := r.PostFormValue("fileName")

	if len(fileName) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		resp.Code = 310
		resp.Msg = "file name not given"
		j, _ := json.Marshal(resp)
		w.Write([]byte(j))
		return
	}

	path := r.PostFormValue("dir")
	if len(path) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		resp.Code = 311
		resp.Msg = "file path not given"
		j, _ := json.Marshal(resp)
		w.Write([]byte(j))
		return
	}

	if !MatchWorkdir(path) {
		w.WriteHeader(http.StatusBadRequest)
		resp.Code = 306
		resp.Msg = "not permitted path"
		j, _ := json.Marshal(resp)
		w.Write([]byte(j))
		return
	}
	// write file
	newFile, err := os.Create(filepath.Join(path, fileName))
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		resp.Code = 312
		resp.Msg = "create file failed"
		j, _ := json.Marshal(resp)
		w.Write([]byte(j))
		return
	}
	defer newFile.Close()
	if _, err := newFile.Write(fileBytes); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		resp.Code = 313
		resp.Msg = "write file failed"
		j, _ := json.Marshal(resp)
		w.Write([]byte(j))
		return
	}
	w.WriteHeader(http.StatusBadRequest)
	resp.Code = 200
	resp.Msg = "ok"
	j, _ := json.Marshal(resp)
	w.Write([]byte(j))
	return
}

func renderError(w http.ResponseWriter, message string, statusCode int) {
	w.WriteHeader(http.StatusBadRequest)
	w.Write([]byte(message))
}

func randToken(len int) string {
	b := make([]byte, len)
	rand.Read(b)
	return fmt.Sprintf("%x", b)
}
