package handlers

import "strings"

func MatchWorkdir(path string) bool {
	if _, ok := Bpara.WorkDir["*"]; ok {
		return true
	}
	if strings.Contains(path, "..") {
		return false
	}
	for workDir, _ := range Bpara.WorkDir {
		if strings.HasPrefix(path, workDir) {
			return true
		}
	}
	return false
}
