package handlers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"os"
)

type C2LReq struct {
	Addr string `json:"addr"`
}

func DownloadFunc(w http.ResponseWriter, r *http.Request) {

	ip := strings.Split(r.RemoteAddr, ":")[0]
	if _, ok := Bpara.AllowIP[ip]; !ok {
		w.Header().Set("code", "307")
		w.Header().Set("msg", fmt.Sprintf("ip %v not allowed", ip))
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	decoder := json.NewDecoder(r.Body)
	var c2lReq = &C2LReq{}
	err := decoder.Decode(c2lReq)
	if err != nil {
		w.Header().Set("code", "301")
		w.Header().Set("msg", "bad request")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	addr := c2lReq.Addr
	if !MatchWorkdir(addr) {
		w.Header().Set("code", "306")
		w.Header().Set("msg", "not permitted path")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if _, err := os.Stat(addr); os.IsNotExist(err) {
		w.Header().Set("code", "312")
		w.Header().Set("msg", "file not exits")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	bytes, err := ioutil.ReadFile(addr)
	if err != nil {
		w.Header().Set("code", "313")
		w.Header().Set("msg", "open file error")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	w.Header().Set("code", "200")
	w.Header().Set("msg", "ok")
	w.WriteHeader(http.StatusOK)
	w.Write(bytes)
	return
}
