package handlers

import (
	"encoding/json"
	"net/http"
	"time"

	"bagent/commons"
)

type BgetResp struct {
	Code   int64  `json:"code"`
	Msg    string `json:"msg"`
	BKey   string `json:"bkey"`
	Osid   string `json:"osid"`
	Key    string `json:"key"`
	Stdout string `json:"stdout"`
	Stderr string `json:"stderr,omitempty"`
	Err    string `json:"err,omitempty"`
}

type BgetReq struct {
	Key        string `json:"key"`
	BagentIP   string `json:"bagent_ip"`
	BagentPort int    `json:"bagent_port"`
}

// 处理 bget 请求
func BgetHandler(w http.ResponseWriter, r *http.Request) {

	resp := &BgetResp{}
	//	fmt.Printf("req:%v", r.Body)
	decoder := json.NewDecoder(r.Body)
	var bget = &BgetReq{}
	err := decoder.Decode(bget)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		resp.Code = 301
		resp.Msg = "bad request"
		j, _ := json.Marshal(resp)
		w.Write([]byte(j))
		return
	}

	key := bget.Key
	if item, ok := KeyMap[key]; !ok {
		w.WriteHeader(http.StatusBadRequest)
		resp.Code = 301
		resp.Msg = "key not exists"
		resp.BKey = bget.Key
	} else {
		w.WriteHeader(http.StatusOK)
		ret := commons.RunCommand(item.Command, 3*time.Second)
		resp.Code = 200
		resp.Msg = "ok"
		resp.Key = key
		resp.Stdout = ret.Stdout
		resp.Stderr = ret.Stderr
		resp.Err = ret.Error
	}
	j, _ := json.Marshal(resp)
	w.Write([]byte(j))
}
