package handlers

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
	"time"
)

type KeyItem struct {
	Interval time.Duration // 自动执行 Key 的间隔
	Command  string        // 命令或者脚本
}

type TypeKeyMap map[string]*KeyItem

var KeyMap = TypeKeyMap{}

// 解析 Key.cnf 文件
func ParseKeyConf(addr string) (err error) {

	fi, err := os.Open(addr)
	if err != nil {
		fmt.Printf("Error: %s\n", err)
		return err
	}
	defer fi.Close()

	buf := bufio.NewReader(fi)
	for {
		lineBytes, _, err := buf.ReadLine()
		if err != nil && err != io.EOF {
			return err
		}
		if io.EOF == err {
			break
		}
		line := string(lineBytes)
		splits := strings.Split(line, "=")
		if len(splits) != 2 {
			fmt.Printf("Error: fmt err of %v\n", line)
			continue
		}
		key := splits[0]
		splits = strings.Split(splits[1], ",")
		if len(splits) < 2 {
			fmt.Printf("Error: fmt err of %v\n", line)
			continue
		}
		intervalSeconds, err := strconv.ParseInt(splits[0], 10, 32)
		if err != nil {
			fmt.Printf("Error: fmt err of %v\n", line)
			continue
		}
		interval := time.Duration(intervalSeconds) * time.Second
		KeyMap[key] = &KeyItem{Interval: interval, Command: splits[1]}
	}
	return
}
