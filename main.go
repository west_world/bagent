package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"time"

	"bagent/configs"
	"bagent/handlers"

	"github.com/gorilla/mux"
)

func main() {
	fmt.Println("bagent starting...")

	// 可以指定各个配置文件的地址，若不指定则默认在 /opt/bagent 下
	flag.StringVar(&configs.BagentConfAddr, "bagent.cnf", "/opt/bagent/bagent.cnf", "bagent config file")
	flag.StringVar(&configs.BparaAddr, "bpara.cnf", "/opt/bagent/bpara.cnf", "bpara config file")
	flag.StringVar(&configs.BkeyAddr, "bkey.cnf", "/opt/bagent/bkey.cnf", "bkey config file")
	flag.StringVar(&configs.ScriptsDir, "scripts", "/opt/bagent/scripts", "scripts dir")
	flag.Parse()

	configs.Config = configs.BagentConf{}
	err := configs.Config.Init()
	if err != nil {
		os.Exit(1)
	}

	handlers.ParseKeyConf(configs.BkeyAddr)
	handlers.ParseBparaConf(configs.BparaAddr)

	// bget 命令
	// url pattern: 127.0.0.1:8090/bget/{key}
	// e.g. key.cnf 中 key 为 today， 命令为 date， curl 127.0.0.1:8090/bget/today 则会返回当前日期
	r := mux.NewRouter()
	r.HandleFunc("/bget", handlers.BgetHandler)
	r.HandleFunc("/bcmd", handlers.BcmdHandler)
	r.HandleFunc("/upload", handlers.UploadFunc)
	r.HandleFunc("/download", handlers.DownloadFunc)
	http.Handle("/", r)

	fmt.Printf("going to start server on %v", configs.Config.Listen+":"+strconv.Itoa(configs.Config.ListenPort))
	// 启动 server
	srv := &http.Server{
		//Addr:    "127.0.0.1:8080",
		Addr: configs.Config.Listen + ":" + strconv.Itoa(configs.Config.ListenPort),
		// Good practice to set timeouts to avoid Slowloris attacks.
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      r, // Pass our instance of gorilla/mux in.
	}

	// Run our server in a goroutine so that it doesn't block.
	go func() {
		if err := srv.ListenAndServe(); err != nil {
			log.Println(err)
		}
	}()
	c := make(chan os.Signal, 1)
	// We'll accept graceful shutdowns when quit via SIGINT (Ctrl+C)
	// SIGKILL, SIGQUIT or SIGTERM (Ctrl+/) will not be caught.
	signal.Notify(c, os.Interrupt)

	// Block until we receive our signal.
	<-c

	// Create a deadline to wait for.
	wait := 5 * time.Second
	ctx, cancel := context.WithTimeout(context.Background(), wait)
	defer cancel()
	// Doesn't block if no connections, but will otherwise wait
	// until the timeout deadline.
	srv.Shutdown(ctx)
	// Optionally, you could run srv.Shutdown in a goroutine and block on
	// <-ctx.Done() if your application should wait for other services
	// to finalize based on context cancellation.
	log.Println("shutting down")
	os.Exit(0)
}
