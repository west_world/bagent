package commons

import "runtime"

var ostype = runtime.GOOS

func Delimeter() string {
	if ostype == "windows" {
		return "\r\n"
	}
	return "\n"
}
