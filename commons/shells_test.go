package commons

import (
	"testing"
	"time"
)

func TestRunCommand(t *testing.T) {
	output := RunCommand("ls", 2*time.Second)
	t.Logf("stdout %v", output.Stdout)
	t.Logf("stderr %v", output.Stderr)
	t.Logf("goerr %v", output.Error)

	output = RunCommand("sleep 5", 2*time.Second)
	t.Logf("stdout %v", output.Stdout)
	t.Logf("stderr %v", output.Stderr)
	t.Logf("goerr %v", output.Error)

}
