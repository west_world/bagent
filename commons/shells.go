package commons

import (
	"bytes"
	"os/exec"
	"regexp"
	"time"
)

// TODO 控制输出的大小
type ShellOutput struct {
	Stdout string
	Stderr string
	Error  string
}

// 执行命令，返回标准输出、标准错误输出、和程序运行错位（如超时等）
// 若超时，则会 kill 执行命令所在的进程
func RunCommand(command string, timeout time.Duration) *ShellOutput {
	output := &ShellOutput{}
	whiteRegexp, _ := regexp.Compile("\\s")
	splits := whiteRegexp.Split(command, -1)
	var stdout bytes.Buffer
	var stderr bytes.Buffer
	var cmd = &exec.Cmd{}
	if len(splits) > 1 {
		cmd = exec.Command(splits[0], splits[1:]...)
	} else {
		cmd = exec.Command(splits[0])
	}

	cmd.Stdout = &stdout
	cmd.Stderr = &stderr

	if err := cmd.Start(); err != nil {
		output.Error = err.Error()
		return output
	}
	ch := make(chan error, 1)
	go func() {
		ch <- cmd.Wait()
	}()
	select {
	case <-time.After(timeout):
		cmd.Process.Kill()
		output.Error = "timeout"
		return output
	case <-ch:
		output.Stderr = stderr.String()
		output.Stdout = stdout.String()
		return output
	}
}
