## commit note 2018.04.25
- 修改配置为 key=value 格式，不用 toml 格式
- 增加 bpara.cnf 文件和解析代码
- 增加 bcmd 相关功能
e.g. conf 下的配置都放到 /opt/bagent/ 下，启动 bagent 二进制文件可以自动读取配置。
curl 127.0.0.1:8090/bcmd -POST -d'{"cmd":"ls /tmp/"}'
正常返回结果
curl 127.0.0.1:8090/bcmd -POST -d'{"cmd":"ls /tmp/../"}'
curl 127.0.0.1:8090/bcmd -POST -d'{"cmd":"du -sh /tmp/"}'
curl 127.0.0.1:8090/bcmd -POST -d'{"cmd":"ls /tmpa/"}'
都返回错误

## commit note 2018.05.05
- 修改 bget 调用方式为 post
e.g. curl 127.0.0.1:8090/bget -POST -d'{"key":"today"}'
- bcmd 增加返回字段 code 和 msg 字段

## commit note 2018.05.10
请求增加 bagent_ip 和 bagent_port 字段

## commit note 2018.06.01
增加处理 copy2bagent 的 handler

## commit note 2018.06.10
增加处理 copy2local 的 handler
修复 bcmd workdir 验证的 bug
